library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

use work.exec_pkg.all;

entity rom_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of rom_tb is

signal rst_s   : std_ulogic;
signal clk_s   : std_ulogic;

signal addr_s : std_ulogic_vector(31 downto 0);
signal we_s : std_ulogic;
signal instruction_s : std_ulogic_vector(31 downto 0);

begin
    u_rom : entity work.mmu_rom
    port map (
        clk_i => clk_s,
        rst_i => rst_s,
        
        addr_i => addr_s,
        we_i => we_s,
        instruction_o => instruction_s
    );

    p_clk : process is
    begin
        clk_s <= '0';
        wait for 10 ns;
        clk_s <= '1';
        wait for 10 ns;

    end process;

    p_test : process is
    variable check_v : std_ulogic_vector(31 downto 0);
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test ROM instructions output") then
            rst_s <= '1';
            we_s <= '0';

            wait until rising_edge(clk_s);
            rst_s <= '0';
            addr_s <= x"00000000";

            wait until rising_edge(clk_s);
            addr_s <= x"00000004";

            wait until rising_edge(clk_s);
            addr_s <= x"00000008";
            check_v := x"00000513";
            check_equal(instruction_s, check_v, "This is the addi x10, x0, 0 instruction.");
            
            wait until rising_edge(clk_s);
            addr_s <= x"0000000C";
            check(instruction_s = x"00100593", "This is the addi x11, x0, 1 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"00000010";
            check(instruction_s = x"00100313", "This is the addi x6, x0, 1 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"00000014";
            check(instruction_s = x"02F00613", "This is the addi x12, x0, 7 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"00000018";
            check(instruction_s = x"00C30063", "This is the beq x6, x12, 0 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"0000001C";
            check(instruction_s = x"00130313", "This is the addi addi x6, x6, 1 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"00000020";
            check(instruction_s = x"00B503B3", "This is the add x7, x10, x11 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"00000024";
            check(instruction_s = x"00058513", "This is the addi x10, x11, 0 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"00000028";
            check(instruction_s = x"00038593", "This is the addi x11, x7, 0 instruction.");

            wait until rising_edge(clk_s);
            addr_s <= x"0000002C";
            check(instruction_s = x"FEDFF06F", "This is the jal x0, -20 instruction.");

            wait until rising_edge(clk_s);
            check(instruction_s = x"00000000", "This is empty.");

            wait until rising_edge(clk_s);
            check(instruction_s = x"00000000", "This is empty.");
        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
