library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

entity and_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of and_tb is

signal data1_s : std_ulogic_vector (3 downto 0);
signal data2_s : std_ulogic_vector (3 downto 0);
signal result_s : std_ulogic_vector (3 downto 0);

begin
    u_and : entity work.alu_and
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        data1_s <= b"1010";
        data2_s <= b"1100";

        wait for 1 ns;
        check(result_s = b"1000", "1010 and 1100 should give 1000");

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
