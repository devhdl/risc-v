library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity sub_tb is
    generic (runner_cfg : string);
end;

architecture tb of sub_tb is
    signal data1_s : unsigned(31 downto 0);
    signal data2_s : unsigned(31 downto 0);
    signal result_s : unsigned(31 downto 0);
begin
    u_sub : entity work.alu_sub
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test normal subtraction") then
                data1_s <= to_unsigned(9, data1_s'length);
                data2_s <= to_unsigned(5, data2_s'length);
                wait for 1 ns;
                check_equal(result_s, 4, "9-5 is expected to be 4");
            elsif run("Test overflow") then
                data1_s <= (others => '0');
                data2_s <= to_unsigned(1, data2_s'length);
                wait for 1 ns;
                check(result_s = (result_s'range => '1'), "0-1 is expected to be max");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;