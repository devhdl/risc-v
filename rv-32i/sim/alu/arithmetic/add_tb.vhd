library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity add_tb is
    generic (runner_cfg : string);
end;

architecture tb of add_tb is
    signal data1_s : unsigned(31 downto 0);
    signal data2_s : unsigned(31 downto 0);
    signal result_s : unsigned(31 downto 0);
begin
    u_add : entity work.alu_add
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test normal addition") then
                data1_s <= to_unsigned(4, data1_s'length);
                data2_s <= to_unsigned(5, data2_s'length);
                wait for 1 ns;
                check_equal(result_s, 9, "4+5 is expected to be 9");
            elsif run("Test overflow") then
                data1_s <= (others => '1');
                data2_s <= to_unsigned(1, data2_s'length);
                wait for 1 ns;
                check_equal(result_s, 0, "max+1 is expected to be 0");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;