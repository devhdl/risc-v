library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

entity sra_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of sra_tb is

signal data1_s : std_ulogic_vector (5 downto 0);
signal data2_s : natural;
signal result_s : std_ulogic_vector (5 downto 0);

begin
    u_sra : entity work.alu_sra
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test sra if MSB is 1") then
            data1_s <= "110011";
            data2_s <= 2;

            wait for 1 ns;
            check(result_s = "111100", "110011 sra 2 should give 111100");
            
        elsif run("Test sra if MSB is 0") then
            data1_s <= "001010";
            data2_s <= 3;

            wait for 1 ns;
            check(result_s = "000001", "001010 sra 3 should give 000001");
        elsif run("Test too many shifts") then
            data1_s <= "001010";
            data2_s <= 7;

            wait for 1 ns;
            check(result_s = "000000", "001010 sra 3 should give 000001");        
        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
