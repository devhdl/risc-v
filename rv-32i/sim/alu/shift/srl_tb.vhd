library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

entity srl_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of srl_tb is

signal data1_s : std_ulogic_vector (5 downto 0);
signal data2_s : natural;
signal result_s : std_ulogic_vector (5 downto 0);

begin
    u_srl : entity work.alu_srl
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);
        
        while test_suite loop

        -- Put common test case setup code here

        if run("Test srl shifts by 2") then
            data1_s <= "110011";
            data2_s <= 2;

            wait for 1 ns;
            check(result_s = "001100", "110011 srl 2 should give 001100");

        elsif run("Test srl shifts by 3") then
            data1_s <= "001010";
            data2_s <= 3;

            wait for 1 ns;
            check(result_s = "000001", "001010 sra 3 should give 000001");
        elsif run("Test too many shifts") then
            data1_s <= "001010";
            data2_s <= 7;

            wait for 1 ns;
            check(result_s = "000000", "001010 sra 3 should give 000001");           
        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
