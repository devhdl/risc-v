library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

use work.exec_pkg.all;

entity alu_comb_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of alu_comb_tb is

signal data1_s : std_ulogic_vector (3 downto 0);
signal data2_s : std_ulogic_vector (3 downto 0);
signal exec_s : exec_t;
signal result_s : std_ulogic_vector (3 downto 0);

begin
    u_alu_comb : entity work.alu_comb
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        exec_i => exec_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test additions") then
            data1_s <= "1100";
            data2_s <= "0010";

            exec_s <= alu_add;

            wait for 10 ns;
            check(result_s = "1110", "1100 + 0010 = 0010");
        
        elsif run("Test subtraction") then
            data1_s <= "1100";
            data2_s <= "0010";

            exec_s <= alu_sub;

            wait for 10 ns;
            check(result_s = "1010", "1100 - 0010 1010");

        elsif run("Test and case") then
            data1_s <= "1100";
            data2_s <= "0110";

            exec_s <= alu_and;

            wait for 10 ns;
            check(result_s = "0100", "1100 and 0110 should be 0100");

        elsif run("Test or case") then
            data1_s <= "1100";
            data2_s <= "0010";

            exec_s <= alu_or;

            wait for 10 ns;
            check(result_s = "1110", "1100 or 0010 should be 1110");
        
        elsif run("Test xor case") then
            data1_s <= "1100";
            data2_s <= "1010";

            exec_s <= alu_xor;

            wait for 10 ns;
            check(result_s = "0110", "1100 xor 1010 should be 0110");
        
        elsif run("Test sll case") then
            data1_s <= "1001";
            data2_s <= "0010";

            exec_s <= alu_sll;

            wait for 10 ns;
            check(result_s = "0100", "1100 sll 2 should be 0100");
        
        elsif run("Test srl case") then
            data1_s <= "1001";
            data2_s <= "0011";

            exec_s <= alu_srl;

            wait for 10 ns;
            check(result_s = "0001", "1001 srl 3 should be 0001");
        
        elsif run("Test sra case 1") then
            data1_s <= "1100";
            data2_s <= "0010";

            exec_s <= alu_sra;

            wait for 10 ns;
            check(result_s = "1111", "1100 sra 2 should be 1111");
        
        elsif run("Test sra case 2") then
            data1_s <= "0100";
            data2_s <= "0011";

            exec_s <= alu_sra;

            wait for 10 ns;
            check(result_s = "0000", "0100 sra 3 should be 0000");

        elsif run("Test slt rs1 > rs2 case") then

            data1_s <= b"0010";
            data2_s <= b"1100";

            exec_s <= alu_slt;

            wait for 1 ns;
            check(result_s = b"0000", "rs1> rs2 set rd = 0");

        elsif run("Test sltu rs1 > rs2 case") then

            data1_s <= b"0010";
            data2_s <= b"0001";

            exec_s <= alu_sltu;

            wait for 1 ns;
            check(result_s = b"0000", "rs1> rs2 set rd = 0");
        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
