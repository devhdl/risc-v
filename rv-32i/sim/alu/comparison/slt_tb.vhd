library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

entity slt_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of slt_tb is

signal data1_s : std_ulogic_vector (3 downto 0);
signal data2_s : std_ulogic_vector (3 downto 0);
signal result_s : std_ulogic_vector (3 downto 0);

begin
    u_slt : entity work.alu_slt
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test slt rs1 < rs2 case") then

            data1_s <= b"0010";
            data2_s <= b"0100";

            wait for 1 ns;
            check(result_s = b"0001", "rs1< rs2 set rd = 1");
        elsif run("Test slt rs1 > rs2 case") then

            data1_s <= b"0010";
            data2_s <= b"1100";

            wait for 1 ns;
            check(result_s = b"0000", "rs1> rs2 set rd = 0");
        end if;

        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
