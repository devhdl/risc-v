library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

use work.exec_pkg.all;

entity alu_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of alu_tb is

signal clk_s : std_ulogic;
signal rst_s : std_ulogic;

signal data1_s : std_ulogic_vector (3 downto 0);
signal data2_s : std_ulogic_vector (3 downto 0);

signal dest_i_s: reg_t;
signal dest_o_s: reg_t;
signal valid_o_s : std_ulogic;
signal valid_i_s : std_ulogic;
signal exec_s : exec_t;
signal result_s : std_ulogic_vector (3 downto 0);

begin
    u_alu : entity work.alu
    port map (
        clk_i => clk_s,
        rst_i => rst_s,
        valid_i => valid_i_s,
        valid_o => valid_o_s,
        data1_i => data1_s,
        data2_i => data2_s,
        exec_i => exec_s,        
        dest_i => dest_i_s,

        result_o => result_s,
        dest_o => dest_o_s
    );

    p_clk : process is
    begin
        clk_s <= '0';
        wait for 10 ns;
        clk_s <= '1';
        wait for 10 ns;

    end process;

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test continuous instructions") then
            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0010";
            exec_s <= alu_add;
            dest_i_s <= 15;

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0110";
            exec_s <= alu_and;
            dest_i_s <= 12;

            wait until rising_edge(clk_s);

            check(result_s = "1110", "1100 + 0010 = 0010");
            check(dest_o_s = 15, "The first register address should be 15.");

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0110";
            exec_s <= alu_sub;
            dest_i_s <= 14;

            wait until rising_edge(clk_s);
            check(result_s = "0100", "1100 and 0110 = 0100");
            check(dest_o_s = 12, "The first register address should be 12.");

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0010";
            exec_s <= alu_sll;
            dest_i_s <= 16;

            wait until rising_edge(clk_s);
            check(result_s = "0110", "1100 - 0110 = 0110");
            check(dest_o_s = 14, "The first register address should be 14.");

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0110";
            exec_s <= alu_or;
            dest_i_s <= 19;

            wait until rising_edge(clk_s);
            check(result_s = "0000", "1100 sll 2 = 0000");
            check(dest_o_s = 16, "The first register address should be 16.");

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0011";
            exec_s <= alu_srl;
            dest_i_s <= 24;

            wait until rising_edge(clk_s);
            check(result_s = "1110", "1100 or 0110 = 1110");
            check(dest_o_s = 19, "The first register address should be 19.");

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0011";
            exec_s <= alu_sra;
            dest_i_s <= 14;

            wait until rising_edge(clk_s);
            check(result_s = "0001", "1100 srl 0011 = 0001");
            check(dest_o_s = 24, "The first register address should be 24.");

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0111";
            exec_s <= alu_xor;
            dest_i_s <= 4;

            wait until rising_edge(clk_s);
            check(result_s = "1111", "1100 sra 0011 = 1111");
            check(dest_o_s = 14, "The first register address should be 14.");

            wait until rising_edge(clk_s);
            check(result_s = "1011", "1100 xor 0011 = 1011");
            check(dest_o_s = 4, "The first register address should be 4.");

        elsif run("Test reset") then
            wait until falling_edge(clk_s);
            rst_s <= '0';
            data1_s <= "1100";
            data2_s <= "0010";
            exec_s <= alu_add;
            dest_i_s <= 15;

            wait until falling_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0110";
            exec_s <= alu_and;
            dest_i_s <= 12;

            wait until rising_edge(clk_s);
            check(result_s = "1110", "1100 + 0010 = 0010");
            check(dest_o_s = 15, "The first register address should be 15.");

            wait until falling_edge(clk_s);
            rst_s <= '1';
            wait until rising_edge(clk_s);
            wait until rising_edge(clk_s);
            check(result_s = "0000", "result is all low when in reset");
            check(dest_o_s = 0, "The register in reset is 0.");
            rst_s <= '0';

            wait until rising_edge(clk_s);
            data1_s <= "1100";
            data2_s <= "0110";
            exec_s <= alu_sub;
            dest_i_s <= 14;

            wait until rising_edge(clk_s);

            wait until rising_edge(clk_s);
            check(result_s = "0110", "1100 - 0110 = 0110");
            check(dest_o_s = 14, "The first register address should be 14.");

        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
