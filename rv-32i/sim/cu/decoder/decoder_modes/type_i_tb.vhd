library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity type_i_tb is
    generic (runner_cfg : string);
end;

architecture tb of type_i_tb is
    signal instruction_s : std_ulogic_vector(31 downto 0);
    signal data_reg_s : reg_array_t(reg_t)(31 downto 0);

    signal data1_s : std_ulogic_vector(31 downto 0);
    signal data2_s : std_ulogic_vector(31 downto 0);
    signal offset_s : std_ulogic_vector(31 downto 0);
    signal op_s : exec_t;
    signal dest_s : reg_t;
begin
    u_type_i : entity work.cu_type_i
    port map (
        instruction_i => instruction_s,
        data_reg_i => data_reg_s,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test addi x9, x20, 3") then
                instruction_s <= "00000000001110100000010010010011";
                for i in 0 to 31 loop
                   data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_add, "This is an add instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
            
            elsif run("Test slli x9, x20, 3") then
                instruction_s <= "00000000001110100001010010010011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_sll, "This is an sll instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
            
            elsif run("Test xori x9, x20, 3") then
                instruction_s <= "00000000001110100100010010010011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_xor, "This is an xor instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test srli x9, x20, 3") then
                instruction_s <= "00000000001110100101010010010011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_srl, "This is an srl instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test srai x9, x20, 3") then
                instruction_s <= "01000000001110100101010010010011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_sra, "This is an sra instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test ori x9, x20, 3") then
                instruction_s <= "00000000001110100110010010010011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_or, "This is an or instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test andi x9, x20, 3") then
                instruction_s <= "00000000001110100111010010010011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_and, "This is an and instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test lb x9, x20, 3") then
                instruction_s <= "00000000001110100000010010000011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_lb, "This is a lb instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test lh x9, x20, 3") then
                instruction_s <= "00000000001110100001010010000011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_lh, "This is a lh instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test lw x9, x20, 3") then
                instruction_s <= "00000000001110100010010010000011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_lw, "This is a lw instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test lbu x9, x20, 3") then
                instruction_s <= "00000000001110100100010010000011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_lbu, "This is a lbu instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test lhu x9, x20, 3") then
                instruction_s <= "00000000001110100101010010000011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_lhu, "This is a lhu instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");
            
            elsif run("Test jalr x9, x20, 3") then
                instruction_s <= "00000000001110100000010011100111";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_jalr, "This is a lh instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;