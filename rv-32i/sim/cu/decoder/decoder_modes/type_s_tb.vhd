library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity type_s_tb is
    generic (runner_cfg : string);
end;

architecture tb of type_s_tb is
    signal instruction_s : std_ulogic_vector(31 downto 0);
    signal data_reg_s : reg_array_t(reg_t)(31 downto 0);

    signal data1_s : std_ulogic_vector(31 downto 0);
    signal data2_s : std_ulogic_vector(31 downto 0);
    signal offset_s : std_ulogic_vector(31 downto 0);
    signal op_s : exec_t;
    signal dest_s : reg_t;
begin
    u_type_s : entity work.cu_type_s
    port map (
        instruction_i => instruction_s,
        data_reg_i => data_reg_s,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test sb x9, x20, 240") then
                instruction_s <= "00001110100110100000100000100011";
                for i in 0 to 31 loop
                   data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_sb, "This is a sb instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(9, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_unsigned(240, 32)), "Offset is 240 in this instruction.");
                check(dest_s = 0, "Store instructions always target x0.");
            
            elsif run("Test sh x9, x20, 240") then
                instruction_s <= "00001110100110100001100000100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_sh, "This is a sh instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(9, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_unsigned(240, 32)), "Offset is 240 in this instruction.");
                check(dest_s = 0, "Store instructions always target x0.");
            
            elsif run("Test sw x9, x20, 240") then
                instruction_s <= "00001110100110100010100000100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = mmu_sw, "This is a sw instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(9, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_unsigned(240, 32)), "Offset is 240 in this instruction.");
                check(dest_s = 0, "Store instructions always target x0.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;