library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity type_b_tb is
    generic (runner_cfg : string);
end;

architecture tb of type_b_tb is
    signal instruction_s : std_ulogic_vector(31 downto 0);
    signal data_reg_s : reg_array_t(reg_t)(31 downto 0);

    signal data1_s : std_ulogic_vector(31 downto 0);
    signal data2_s : std_ulogic_vector(31 downto 0);
    signal offset_s : std_ulogic_vector(31 downto 0);
    signal op_s : exec_t;
    signal dest_s : reg_t;
begin
    u_type_b : entity work.cu_type_b
    port map (
        instruction_i => instruction_s,
        data_reg_i => data_reg_s,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test beq x10, x11, 2000") then
                instruction_s <= "01111100101101010000100001100011";
                for i in 0 to 31 loop
                   data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_beq, "This is a beq instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");
            
            elsif run("Test bne x10, x11, 2000") then
                instruction_s <= "01111100101101010001100001100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_bne, "This is a bne instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");
            
            elsif run("Test blt x10, x11, 2000") then
                instruction_s <= "01111100101101010100100001100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_blt, "This is a blt instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");

            elsif run("Test bge x10, x11, 2000") then
                instruction_s <= "01111100101101010101100001100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_bge, "This is a bge instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");
            
            elsif run("Test bltu x10, x11, 2000") then
                instruction_s <= "01111100101101010110100001100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_bltu, "This is a bltu instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");

            elsif run("Test bgeu x10, x11, 2000") then
                instruction_s <= "01111100101101010111100001100011";
                for i in 0 to 31 loop
                data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_bgeu, "This is a bgeu instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;