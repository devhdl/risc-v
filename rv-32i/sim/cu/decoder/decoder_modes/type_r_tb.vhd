library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity type_r_tb is
    generic (runner_cfg : string);
end;

architecture tb of type_r_tb is
    signal instruction_s : std_ulogic_vector(31 downto 0);
    signal data_reg_s : reg_array_t(reg_t)(31 downto 0);

    signal data1_s : std_ulogic_vector(31 downto 0);
    signal data2_s : std_ulogic_vector(31 downto 0);
    signal offset_s : std_ulogic_vector(31 downto 0);
    signal op_s : exec_t;
    signal dest_s : reg_t;
begin
    u_type_r : entity work.cu_type_r
    port map (
        instruction_i => instruction_s,
        data_reg_i => data_reg_s,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test add x9, x20, x21") then
                instruction_s <= "00000001010110100000010010110011";
                for i in 0 to 31 loop
                   data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_add, "This is an add instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(21, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
            
            elsif run("Test sub x9, x20, x21") then
                instruction_s <= "01000001010110100000010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_sub, "This is a sub instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(21, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
            
            elsif run("Test sll x9, x20, x4") then
                instruction_s <= "00000000010010100001010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_sll, "This is a sll instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x4 = 4.");
                check(data2_s = std_ulogic_vector(to_unsigned(4, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test xor x9, x20, x21") then
                instruction_s <= "00000001010110100100010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_xor, "This is a xor instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(21, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
                
            elsif run("Test srl x9, x20, x4") then
                instruction_s <= "00000000010010100101010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_srl, "This is a srl instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(4, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
            
            elsif run("Test sra x9, x20, x4") then
                instruction_s <= "01000000010010100101010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_sra, "This is a sra instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(4, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test or x9, x20, x21") then
                instruction_s <= "00000001010110100110010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_or, "This is a or instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(21, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");

            elsif run("Test and x9, x20, x21") then
                instruction_s <= "00000001010110100111010010110011";
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = alu_and, "This is a xor instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(21, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;