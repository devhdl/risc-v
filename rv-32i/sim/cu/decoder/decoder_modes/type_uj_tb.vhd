library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity type_uj_tb is
    generic (runner_cfg : string);
end;

architecture tb of type_uj_tb is
    signal instruction_s : std_ulogic_vector(31 downto 0);
    signal data_reg_s : reg_array_t(reg_t)(31 downto 0);

    signal data1_s : std_ulogic_vector(31 downto 0);
    signal data2_s : std_ulogic_vector(31 downto 0);
    signal offset_s : std_ulogic_vector(31 downto 0);
    signal op_s : exec_t;
    signal dest_s : reg_t;
begin
    u_type_uj : entity work.cu_type_uj
    port map (
        instruction_i => instruction_s,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test jal x1, 2000") then
                instruction_s <= "01111101000000000000000011101111";
                for i in 0 to 31 loop
                   data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                wait for 1 ns;
                check(op_s = cu_jal, "This is a sb instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(0, 32)), "Data1 is 0 in JAL.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in JAL.");
                check(offset_s = std_ulogic_vector(to_signed(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 1, "Destination is set at x1.");

            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;