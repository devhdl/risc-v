library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity decoder_tb is
    generic (runner_cfg : string);
end;

architecture tb of decoder_tb is

    signal rst_s   : std_ulogic;
    signal clk_s   : std_ulogic;

    signal valid_i_s : std_ulogic;
    signal valid_o_s : std_ulogic;

    signal pc_i_s : std_ulogic_vector(3 downto 0);
    signal pc_o_s : std_ulogic_vector(3 downto 0);

    signal instruction_s : std_ulogic_vector(31 downto 0);
    signal data_reg_s : reg_array_t(reg_t)(31 downto 0);

    signal data1_s : std_ulogic_vector(31 downto 0);
    signal data2_s : std_ulogic_vector(31 downto 0);
    signal offset_s : std_ulogic_vector(31 downto 0);
    signal op_s : exec_t;
    signal dest_s : reg_t;

begin
    u_decoder : entity work.cu_decoder
    port map (
        rst_i => rst_s,
        clk_i => clk_s,

        valid_i => valid_i_s,

        pc_i => pc_i_s,
        instruction_i => instruction_s,
        data_reg_i => data_reg_s,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s,
        pc_o => pc_o_s,
        valid_o => valid_o_s
    );

    p_clk : process is
        begin
            clk_s <= '0';
            wait for 10 ns;
            clk_s <= '1';
            wait for 10 ns;   
        end process;

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Reset then continuous instructions") then
                rst_s <= '1';
                for i in 0 to 31 loop
                    data_reg_s(i) <= std_ulogic_vector(to_unsigned(i, 32));
                end loop;
                pc_i_s <= "0000";
                valid_i_s <= '0';
                
                wait until rising_edge(clk_s);
                rst_s <= '0';
                instruction_s <= "01111100101101010000100001100011";
                pc_i_s <= "0010";
                valid_i_s <= '1';

                wait until rising_edge(clk_s);
                instruction_s <= "00000000001110100101010010000011";
                pc_i_s <= "0011";
                valid_i_s <= '1';

                check(op_s = none, "No operation coming out from reset.");
                check(data1_s = std_ulogic_vector(to_unsigned(0, 32)), "Data1 is 0 coming out from reset.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 coming out from reset.");
                check(offset_s = std_ulogic_vector(to_unsigned(0, 32)), "Offset is 0 coming out from reset.");
                check(dest_s = 0, "Dest is 0 coming out from reset.");
                check(pc_o_s = "0000");

                wait until rising_edge(clk_s);
                instruction_s <= "00000000001110100100010010010011";
                pc_i_s <= "0111";
                valid_i_s <= '1';

                check(op_s = cu_beq, "This is a beq instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(10, 32)), "This is the data rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(11, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_unsigned(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 0, "Branching instructions always target x0.");
                check(pc_o_s = "0010");
            
                wait until rising_edge(clk_s);
                instruction_s <= "00000000001110100000010011100111";
                pc_i_s <= "1111";
                valid_i_s <= '1';

                check(op_s = mmu_lhu, "This is a lhu instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");
                check(pc_o_s = "0011");

                wait until rising_edge(clk_s);
                instruction_s <= "00000001010110100100010010110011";
                pc_i_s <= "0010";
                valid_i_s <= '1';

                check(op_s = alu_xor, "This is an xor instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(3, 32)), "This is the data from immediate, imm12 = 3.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
                check(pc_o_s = "0111");

                wait until rising_edge(clk_s);
                instruction_s <= "00001110100110100010100000100011";
                pc_i_s <= "1110";
                valid_i_s <= '1';

                check(op_s = cu_jalr, "This is a lh instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in all load operations.");
                check(offset_s = std_ulogic_vector(to_unsigned(3, 32)), "Offset is 3 in this instruction.");
                check(dest_s = 9, "This is the destination x9.");
                check(pc_o_s = "1111");
            
                wait until rising_edge(clk_s);
                instruction_s <= "01111101000000000000000011101111";
                pc_i_s <= "1010";
                valid_i_s <= '1';

                check(op_s = alu_xor, "This is a xor instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the data of rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(21, 32)), "This is the data of rs2, x21 = 21.");
                check(offset_s = (offset_s'range => '0'), "Offset is 0 in all ALU operations.");
                check(dest_s = 9, "This is the destination x9.");
                check(pc_o_s = "0010");

                wait until rising_edge(clk_s);
                instruction_s <= "01000001010110100111010010110011";
                pc_i_s <= "0110";
                valid_i_s <= '1';

                check(op_s = mmu_sw, "This is a sw instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(20, 32)), "This is the base address stored in rs1, x20 = 20.");
                check(data2_s = std_ulogic_vector(to_unsigned(9, 32)), "This is the data of rs2, x9 = 9.");
                check(offset_s = std_ulogic_vector(to_unsigned(240, 32)), "Offset is 240 in this instruction.");
                check(dest_s = 0, "Store instructions always target x0.");
                check(pc_o_s = "1110");
            
                wait until rising_edge(clk_s);
                instruction_s <= "01000001010110100111010010110011";
                pc_i_s <= "0011";
                valid_i_s <= '1';

                check(op_s = cu_jal, "This is a sb instruction.");
                check(data1_s = std_ulogic_vector(to_unsigned(0, 32)), "Data1 is 0 in JAL.");
                check(data2_s = std_ulogic_vector(to_unsigned(0, 32)), "Data2 is 0 in JAL.");
                check(offset_s = std_ulogic_vector(to_unsigned(2000, 32)), "Offset is 2000 in this instruction.");
                check(dest_s = 1, "Destination is set at x1.");
                check(pc_o_s = "1010");

            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;