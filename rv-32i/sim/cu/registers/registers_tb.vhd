library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

use work.exec_pkg.all;

entity registers_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of registers_tb is

signal rst_s   : std_ulogic;
signal clk_s   : std_ulogic;

signal data_s : std_ulogic_vector (3 downto 0);
signal reg_s   : reg_t;
signal valid_s : std_ulogic;

signal data_all_s : reg_array_t(reg_t)(3 downto 0);

begin
    u_registers : entity work.cu_registers
    port map (
        clk_i => clk_s,
        rst_i => rst_s,
        data_i => data_s,
        reg_i => reg_s,
        valid_i => valid_s,
        data_all_o => data_all_s
    );

    p_clk : process is
    begin
        clk_s <= '0';
        wait for 10 ns;
        clk_s <= '1';
        wait for 10 ns;

    end process;

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test register reset and store") then
            rst_s <= '1';
            wait until rising_edge(clk_s);
            rst_s <= '0';
            reg_s <= 4;
            data_s <= "1010";
            valid_s <= '1';
            wait until rising_edge(clk_s);
            check(data_all_s(0) = "0000", "All registers should have only zeroes after reset 0");
            check(data_all_s(10) = "0000", "All registers should have only zeroes after reset 10");
            check(data_all_s(20) = "0000", "All registers should have only zeroes after reset 20");
            check(data_all_s(30) = "0000", "All registers should have only zeroes after reset 30");
            
            wait until rising_edge(clk_s);
            check(data_all_s(0) = "0000", "x0 should be zeroes all the time.");
            check(data_all_s(4) = "1010", "register 4 should now be set as 1010");
        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
