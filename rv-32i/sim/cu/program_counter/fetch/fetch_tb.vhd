library ieee;
library vunit_lib;
context vunit_lib.vunit_context;
use ieee.STD_LOGIC_1164.ALL;

use work.exec_pkg.all;

entity fetch_tb is
    generic (runner_cfg : string);
end;

architecture behavioral of fetch_tb is

signal rst_s   : std_ulogic;
signal clk_s   : std_ulogic;

signal pc_i_s   : std_ulogic_vector(31 downto 0);
signal valid_i_s  : std_ulogic;
signal branch_valid_i_s : std_ulogic;

signal ext_addr_o_s : std_ulogic_vector(31 downto 0);
signal ext_we_o_s   : std_ulogic;        
signal ext_instruction_i_s : std_ulogic_vector(31 downto 0);

signal instruction_o_s : std_ulogic_vector(31 downto 0);
signal pc_o_s : std_ulogic_vector(31 downto 0);
signal valid_o_s : std_ulogic;

begin
    u_fetch : entity work.cu_fetch
    port map (
        clk_i => clk_s,
        rst_i => rst_s,
        pc_i => pc_i_s,
        valid_i => valid_i_s,
        branch_valid_i => branch_valid_i_s,
        ext_addr_o => ext_addr_o_s,
        ext_we_o => ext_we_o_s,
        ext_instruction_i => ext_instruction_i_s,
        instruction_o => instruction_o_s,
        pc_o => pc_o_s,
        valid_o => valid_o_s 
    );

    p_clk : process is
    begin
        clk_s <= '0';
        wait for 10 ns;
        clk_s <= '1';
        wait for 10 ns;

    end process;

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop

        -- Put common test case setup code here

        if run("Test fetch logic after reset") then
            rst_s <= '1';
            wait until rising_edge(clk_s);

            wait until rising_edge(clk_s);
            rst_s <= '0';
            pc_i_s <= x"00000000";
            valid_i_s <= '0';
            branch_valid_i_s <= '0';

            check(valid_o_s = '0', "Valid is low during reset.");
            check(pc_o_s = x"00000000", "pc_o is all low during reset");
            check(instruction_o_s = x"00000000", "instruction_o is all low during reset");
            
            wait until rising_edge(clk_s);  -- Cycle accepting data
            check(valid_o_s = '0', "Valid is low coming out of reset.");
            
            wait until rising_edge(clk_s);  -- Cycle sending ext_addr
            check(valid_o_s = '0', "Valid is low when instruction is not ready (1/4).");
            check(ext_addr_o_s = x"00000000", "Retrieving first instruction from ROM coming out of reset.");

            wait until rising_edge(clk_s);  -- Cycle ROM accepting address
            check(valid_o_s = '0', "Valid is low when instruction is not ready (2/4).");

            wait until rising_edge(clk_s);  -- Cycle ROM releasing instructions
            ext_instruction_i_s <= x"00000513";
            check(valid_o_s = '0', "Valid is low when instruction is not ready (3/4).");

            wait until rising_edge(clk_s);  -- Cycle fetch receiving instructions
            check(valid_o_s = '0', "Valid is low when instruction is not ready (4/4).");

            wait until rising_edge(clk_s);  -- Cycle fetch releasing instructions
            check(valid_o_s = '1', "Valid is high when valid instructions are released.");
            check(instruction_o_s = x"00000513", "Instruction is released.");
            check(pc_o_s = x"00000000", "First PC is 0.");

            wait until rising_edge(clk_s);
            check(valid_o_s = '0', "Valid is low when idle(1/4).");

            wait until rising_edge(clk_s);
            check(valid_o_s = '0', "Valid is low when idle(2/4).");

            wait until rising_edge(clk_s);
            check(valid_o_s = '0', "Valid is low when idle(3/4).");

            wait until rising_edge(clk_s);
            pc_i_s <= x"00000008";
            valid_i_s <= '1';
            branch_valid_i_s <= '0';
            check(valid_o_s = '0', "Valid is low when idle(4/4).");

            wait until rising_edge(clk_s);  -- Cycle accepting data
            valid_i_s <= '0';
            branch_valid_i_s <= '0';
            check(valid_o_s = '0', "Valid is low accepting data.");
            
            wait until rising_edge(clk_s);  -- Cycle sending ext_addr
            check(valid_o_s = '0', "Valid is low when instruction is not ready (1/4).");
            check(ext_addr_o_s = x"00000004", "Retrieving 2nd instruction from ROM coming out of reset.");

            wait until rising_edge(clk_s);  -- Cycle ROM accepting address
            check(valid_o_s = '0', "Valid is low when 2nd instruction is not ready (2/4).");

            wait until rising_edge(clk_s);  -- Cycle ROM releasing instructions
            ext_instruction_i_s <= x"00B503B3";
            check(valid_o_s = '0', "Valid is low when 2nd instruction is not ready (3/4).");

            wait until rising_edge(clk_s);  -- Cycle fetch receiving instructions
            check(valid_o_s = '0', "Valid is low when 2nd instruction is not ready (4/4).");

            wait until rising_edge(clk_s);  -- Cycle fetch releasing instructions
            check(valid_o_s = '1', "Valid is high when valid instructions are released.");
            check(instruction_o_s = x"00B503B3", "Instruction 2 is released.");
            check(pc_o_s = x"00000004", "Second PC is 4.");

            wait until rising_edge(clk_s);
            check(valid_o_s = '0', "Valid is low when idle 2 (1/4).");

            wait until rising_edge(clk_s);
            check(valid_o_s = '0', "Valid is low when idle 2 (2/4).");

            wait until rising_edge(clk_s);
            check(valid_o_s = '0', "Valid is low when idle 2 (3/4).");

            wait until rising_edge(clk_s);
            pc_i_s <= x"0000000C";
            valid_i_s <= '1';
            branch_valid_i_s <= '1';
            check(valid_o_s = '0', "Valid is low when idle 2 (4/4).");

            wait until rising_edge(clk_s);  -- Cycle accepting data
            valid_i_s <= '0';
            branch_valid_i_s <= '0';
            check(valid_o_s = '0', "Valid is low accepting data 2.");
            
            wait until rising_edge(clk_s);  -- Cycle sending ext_addr
            check(valid_o_s = '0', "Valid is low when instruction is not ready (1/4).");
            check(ext_addr_o_s = x"0000000C", "Retrieving 3rd instruction from ROM coming out of reset.");

            wait until rising_edge(clk_s);  -- Cycle ROM accepting address
            check(valid_o_s = '0', "Valid is low when 3rd instruction is not ready (2/4).");

            wait until rising_edge(clk_s);  -- Cycle ROM releasing instructions
            ext_instruction_i_s <= x"FEDFF06F";
            check(valid_o_s = '0', "Valid is low when 3rd instruction is not ready (3/4).");

            wait until rising_edge(clk_s);  -- Cycle fetch receiving instructions
            check(valid_o_s = '0', "Valid is low when 3rd instruction is not ready (4/4).");

            wait until rising_edge(clk_s);  -- Cycle fetch releasing instructions
            check(valid_o_s = '1', "Valid is high when valid instructions are released.");
            check(instruction_o_s = x"FEDFF06F", "Instruction 3 is released.");
            check(pc_o_s = x"0000000C", "Third PC is branched to 12.");
            
        end if;

        -- Put common test case cleanup code here

        end loop;

        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end;
