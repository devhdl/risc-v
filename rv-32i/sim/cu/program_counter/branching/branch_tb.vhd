library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity branch_tb is
    generic (runner_cfg : string);
end;

architecture tb of branch_tb is

    signal rst_s   : std_ulogic;
    signal clk_s   : std_ulogic;

    signal valid_i_s : std_ulogic;
    signal valid_o_s : std_ulogic;
    signal bvalid_s : std_ulogic;

    signal pc_i_s : std_ulogic_vector(3 downto 0);
    signal pc_o_s : std_ulogic_vector(3 downto 0);

    signal data1_s : std_ulogic_vector(3 downto 0);
    signal data2_s : std_ulogic_vector(3 downto 0);
    signal offset_s : std_ulogic_vector(3 downto 0);
    signal result_s : std_ulogic_vector(3 downto 0);
    signal exec_s : exec_t;
    signal dest_i_s : reg_t;
    signal dest_o_s : reg_t;

begin
    u_branch : entity work.cu_branch
    port map (
        rst_i => rst_s,
        clk_i => clk_s,

        valid_i => valid_i_s,
        dest_i => dest_i_s,

        data1_i => data1_s,
        data2_i => data2_s,
        offset_i => offset_s,
        pc_i => pc_i_s,

        exec_i => exec_s,

        result_o => result_s,
        branch_valid_o => bvalid_s,
        valid_o => valid_o_s,
        pc_o => pc_o_s,
        dest_o => dest_o_s
    );

    p_clk : process is
        begin
            clk_s <= '0';
            wait for 10 ns;
            clk_s <= '1';
            wait for 10 ns;   
        end process;

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Reset then continuous instructions") then
                rst_s <= '1';
                
                wait until rising_edge(clk_s);
                rst_s <= '0';
                exec_s <= cu_beq;
                data1_s <= "0000";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                valid_i_s <= '1';
                dest_i_s <= 0;

                wait until rising_edge(clk_s);
                exec_s <= cu_bgeu;   
                data1_s <= "0010";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                valid_i_s <= '1';
                dest_i_s <= 0;
                check(pc_o_s = "0000", "PC out is zeroes coming out of reset.");
                check(bvalid_s = '0', "bvalid is low coming out of reset.");
                check(valid_o_s = '0', "valid is low coming out of reset.");
                check(result_s = "0000", "Result is 0 coming out of reset.");
                check(dest_o_s = 0, "Dest point to 0 coming out of reset.");

                wait until rising_edge(clk_s);
                exec_s <= cu_jal;
                data1_s <= "0000";
                data2_s <= "0000"; 
                pc_i_s <= "0011";
                offset_s <= "1110";
                valid_i_s <= '1';
                dest_i_s <= 4;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 != data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");
                check(valid_o_s = '1', "valid is high.");
                check(dest_o_s = 0, "Dest is set as register 0.");

                wait until rising_edge(clk_s);
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 > data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");
                check(valid_o_s = '1', "valid is high.");
                check(dest_o_s = 0, "Dest is set as register 0.");

                wait until rising_edge(clk_s);
                check(pc_o_s = "0001", "PC is added by the offset.");
                check(bvalid_s = '1', "JAL gives valid branching.");
                check(result_s = "0111", "JAL stores instruction following the jump.");
                check(valid_o_s = '1', "valid is high.");
                check(dest_o_s = 4, "Dest is set as register 0.");
                
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;