library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity branch_comb_tb is
    generic (runner_cfg : string);
end;

architecture tb of branch_comb_tb is
    signal data1_s : std_ulogic_vector(3 downto 0);
    signal data2_s : std_ulogic_vector(3 downto 0);
    signal offset_s : std_ulogic_vector(3 downto 0);
    signal pc_i_s : std_ulogic_vector(3 downto 0);

    signal exec_s : exec_t;

    signal bvalid_s : std_ulogic;
    signal result_s : std_ulogic_vector(3 downto 0);
    signal pc_o_s : std_ulogic_vector(3 downto 0);

begin
    u_branch_comb : entity work.cu_branch_comb
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        offset_i => offset_s,
        pc_i => pc_i_s,

        exec_i => exec_s,

        result_o => result_s,
        branch_valid_o => bvalid_s,
        pc_o => pc_o_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test unequal cases (beq, branch_comb)") then
                exec_s <= cu_beq;
                data1_s <= "0000";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 != data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");
            
            elsif run("Test equal cases (beq, branch_comb)") then
                exec_s <= cu_beq;
                data1_s <= "0000";
                data2_s <= "0000";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 = data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");
            
            elsif run("Test unequal cases (bne, branch_comb)") then
                exec_s <= cu_bne;
                data1_s <= "0000";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 != data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test equal cases (bne, branch_comb)") then
                exec_s <= cu_bne;
                data1_s <= "0000";
                data2_s <= "0000";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 = data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test negative offset (bne, branch_comb)") then
                exec_s <= cu_bne;
                data1_s <= "0000";
                data2_s <= "0000";
                pc_i_s <= "0101";
                offset_s <= "1110";
                wait for 1 ns;
                check(pc_o_s = "0011", "PC is added by the offset 5-2 = 3.");
                check(bvalid_s = '0', "data1 = data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test greater than cases with 2 unsigned (bge, branch_comb)") then
                exec_s <= cu_bge;
                data1_s <= "0010";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 > data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test greater than cases with 2 signed (bge, branch_comb)") then
                exec_s <= cu_bge;
                data1_s <= "0001";
                data2_s <= "1000";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 > data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test smaller than cases with 2 unsigned (bge, branch_comb)") then
                exec_s <= cu_bge;
                data1_s <= "0001";
                data2_s <= "0010";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 < data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test smaller than cases with 2 signed (bge, branch_comb)") then
                exec_s <= cu_bge;
                data1_s <= "1001";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 < data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test equal case (bge, branch_comb)") then
                exec_s <= cu_bge;
                data1_s <= "0001";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 = data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test greater than cases with 2 unsigned (blt, branch_comb)") then
                exec_s <= cu_blt;
                data1_s <= "0010";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 > data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test greater than cases with 2 signed (blt, branch_comb)") then
                exec_s <= cu_blt;
                data1_s <= "0001";
                data2_s <= "1000";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 > data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test smaller than cases with 2 unsigned (blt, branch_comb)") then
                exec_s <= cu_blt;
                data1_s <= "0001";
                data2_s <= "0010";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 < data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test smaller than cases with 2 signed (blt, branch_comb)") then
                exec_s <= cu_blt;    
                data1_s <= "1001";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 < data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test equal case (blt, branch_comb)") then
                exec_s <= cu_blt; 
                data1_s <= "0001";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 = data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test greater than cases with 2 unsigned (bgeu, branch_comb)") then
                exec_s <= cu_bgeu;   
                data1_s <= "0010";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 > data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test smaller than cases with 2 unsigned (bgeu, branch_comb)") then
                exec_s <= cu_bgeu;       
                data1_s <= "0001";
                data2_s <= "0010";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 < data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test equal case (bgeu, branch_comb)") then
                exec_s <= cu_bgeu;       
                data1_s <= "0001";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 = data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test greater than cases with 2 unsigned (bltu, branch_comb)") then
                exec_s <= cu_bltu;
                data1_s <= "0010";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 > data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test smaller than cases with 2 unsigned (bltu, branch_comb)") then
                exec_s <= cu_bltu;    
                data1_s <= "0001";
                data2_s <= "0010";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 < data2 gives valid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test equal case (bltu, branch_comb)") then
                exec_s <= cu_bltu;    
                data1_s <= "0001";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 = data2 gives invalid branching.");
                check(result_s = "0000", "Result is 0 when conditional branching.");

            elsif run("Test positive offset (jal, branch_comb)") then
                exec_s <= cu_jal;  
                pc_i_s <= "0001";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0011", "PC is added by the offset.");
                check(bvalid_s = '1', "JAL gives valid branching.");
                check(result_s = "0101", "JAL stores instruction following the jump.");
            
            elsif run("Test negative offset (jal, branch_comb)") then
                exec_s <= cu_jal; 
                pc_i_s <= "0011";
                offset_s <= "1110";
                wait for 1 ns;
                check(pc_o_s = "0001", "PC is added by the offset.");
                check(bvalid_s = '1', "JAL gives valid branching.");
                check(result_s = "0111", "JAL stores instruction following the jump.");

            elsif run("Test positive offset (jalr, branch_comb)") then
                exec_s <= cu_jalr; 
                data1_s <= "0100";
                pc_i_s <= "0001";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0110", "PC is added by the offset.");
                check(bvalid_s = '1', "JALR gives valid branching.");
                check(result_s = "0101", "JALR stores instruction following the jump.");

            elsif run("Test negative offset (jalr, branch_comb)") then
                exec_s <= cu_jalr; 
                data1_s <= "0100";
                pc_i_s <= "0010";
                offset_s <= "1110";
                wait for 1 ns;
                check(pc_o_s = "0010", "PC is added by the offset.");
                check(bvalid_s = '1', "JALR gives valid branching.");
                check(result_s = "0110", "JALR stores instruction following the jump.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;