library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity beq_tb is
    generic (runner_cfg : string);
end;

architecture tb of beq_tb is
    signal data1_s : std_ulogic_vector(3 downto 0);
    signal data2_s : std_ulogic_vector(3 downto 0);
    signal offset_s : std_ulogic_vector(3 downto 0);
    signal pc_i_s : std_ulogic_vector(3 downto 0);

    signal bvalid_s : std_logic;
    signal pc_o_s : std_ulogic_vector(3 downto 0);

begin
    u_beq : entity work.cu_beq
    port map (
        data1_i => data1_s,
        data2_i => data2_s,
        offset_i => offset_s,
        pc_i => pc_i_s,

        bvalid_o => bvalid_s,
        pc_o => pc_o_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test unequal cases (beq)") then
                data1_s <= "0000";
                data2_s <= "0001";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '0', "data1 != data2 gives invalid branching.");
            elsif run("Test equal cases (beq)") then
                data1_s <= "0000";
                data2_s <= "0000";
                pc_i_s <= "0101";
                offset_s <= "0010";
                wait for 1 ns;
                check(pc_o_s = "0111", "PC is added by the offset.");
                check(bvalid_s = '1', "data1 = data2 gives valid branching.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;