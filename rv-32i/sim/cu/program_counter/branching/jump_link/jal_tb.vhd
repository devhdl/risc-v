library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity jal_tb is
    generic (runner_cfg : string);
end;

architecture tb of jal_tb is
    signal offset_s : std_ulogic_vector(4 downto 0);
    signal pc_i_s : std_ulogic_vector(4 downto 0);

    signal bvalid_s : std_logic;
    signal pc_o_s : std_ulogic_vector(4 downto 0);
    signal result_s : std_ulogic_vector(4 downto 0);

begin
    u_jal : entity work.cu_jal
    port map (
        offset_i => offset_s,
        pc_i => pc_i_s,

        bvalid_o => bvalid_s,
        pc_o => pc_o_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test positive offset (jal)") then
                pc_i_s <= "00101";
                offset_s <= "00010";
                wait for 1 ns;
                check(pc_o_s = "00111", "PC is added by the offset.");
                check(bvalid_s = '1', "JAL gives valid branching.");
                check(result_s = "01001", "JAL stores instruction following the jump.");
            elsif run("Test negative offset (jal)") then
                pc_i_s <= "00101";
                offset_s <= "11110";
                wait for 1 ns;
                check(pc_o_s = "00011", "PC is added by the offset.");
                check(bvalid_s = '1', "JAL gives valid branching.");
                check(result_s = "01001", "JAL stores instruction following the jump.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;