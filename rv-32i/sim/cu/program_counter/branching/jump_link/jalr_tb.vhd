library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity jalr_tb is
    generic (runner_cfg : string);
end;

architecture tb of jalr_tb is
    signal data1_s : std_ulogic_vector(4 downto 0);
    signal offset_s : std_ulogic_vector(4 downto 0);
    signal pc_i_s : std_ulogic_vector(4 downto 0);

    signal bvalid_s : std_logic;
    signal pc_o_s : std_ulogic_vector(4 downto 0);
    signal result_s : std_ulogic_vector(4 downto 0);

begin
    u_jalr : entity work.cu_jalr
    port map (
        data1_i => data1_s,
        offset_i => offset_s,
        pc_i => pc_i_s,

        bvalid_o => bvalid_s,
        pc_o => pc_o_s,
        result_o => result_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test positive offset (jalr)") then
                data1_s <= "00011";
                pc_i_s <= "00101";
                offset_s <= "00010";
                wait for 1 ns;
                check(pc_o_s = "00100", "PC is added by the offset.");
                check(bvalid_s = '1', "JALR gives valid branching.");
                check(result_s = "01001", "JALR stores instruction following the jump.");
            elsif run("Test negative offset (jalr)") then
                data1_s <= "01110";
                pc_i_s <= "00101";
                offset_s <= "11110";
                wait for 1 ns;
                check(pc_o_s = "01100", "PC is added by the offset.");
                check(bvalid_s = '1', "JALR gives valid branching.");
                check(result_s = "01001", "JALR stores instruction following the jump.");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;