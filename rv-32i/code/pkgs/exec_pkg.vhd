library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

package exec_pkg is
    type exec_t is (alu_add, alu_sub, alu_and, alu_or, alu_xor, alu_sll, alu_sra, alu_srl, alu_slt, alu_sltu,
                    mmu_sb, mmu_sh, mmu_sw, mmu_lb, mmu_lh, mmu_lw, mmu_lbu, mmu_lhu,
                    cu_beq, cu_bne, cu_blt, cu_bge, cu_bltu, cu_bgeu, cu_jal, cu_jalr,
                    -- lui, auipc
                    none);

    subtype reg_t is natural range 0 to 31;
    type reg_array_t is array (natural range <>) of std_ulogic_vector;

    function to_reg_t(
        bin_reg_t : in std_logic_vector)
    return reg_t;
end package;

package body exec_pkg is
    function to_reg_t(
        bin_reg_t : in std_logic_vector)
    return reg_t is
    begin
        if to_integer(unsigned(bin_reg_t)) < 32 then
            return to_integer(unsigned(bin_reg_t));
        else
            return 0;
        end if;
    end;
end package body;