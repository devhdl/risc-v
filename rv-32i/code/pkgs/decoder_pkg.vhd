library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use work.exec_pkg.all;

package decoder_pkg is
    function imm20_to_32b_ui(
        immediate_i : in std_ulogic_vector(19 downto 0)
    )
    return std_ulogic_vector;

    function imm12_to_32b (
        immediate_i : in std_ulogic_vector(11 downto 0)
    )
    return std_ulogic_vector;

    function shamt_to_32b (
        shamt_i : in std_ulogic_vector(4 downto 0)
    )
    return std_ulogic_vector;

    function alu_type (
        exec_i : in exec_t
    )
    return std_ulogic;

    function branch_type (
        exec_i : in exec_t
    )
    return std_ulogic;

    function mmu_type (
        exec_i : in exec_t
    )
    return std_ulogic;

    constant op_imm_op : std_ulogic_vector(6 downto 0) := "0010011";
    -- constant lui_op : std_ulogic_vector(6 downto 0) := "0110111";
    -- constant auipc_op : std_ulogic_vector(6 downto 0) := "0010111";
    constant op_op : std_ulogic_vector(6 downto 0) := "0110011";
    constant jal_op : std_ulogic_vector(6 downto 0) := "1101111";
    constant jalr_op : std_ulogic_vector(6 downto 0) := "1100111";
    constant branch_op : std_ulogic_vector(6 downto 0) := "1100011";
    constant load_op : std_ulogic_vector(6 downto 0) := "0000011";
    constant store_op : std_ulogic_vector(6 downto 0) := "0100011";


end package;

package body decoder_pkg is
    function imm20_to_32b_ui (
        immediate_i : in std_ulogic_vector(19 downto 0)
    )
    return std_ulogic_vector is
    begin
        return immediate_i & (11 downto 0 => '0');
    end;

    function imm12_to_32b (
        immediate_i : in std_ulogic_vector(11 downto 0)
    )
    return std_ulogic_vector is
    begin
        return std_ulogic_vector(to_signed(to_integer(signed(immediate_i)),32));
    end;

    function shamt_to_32b (
        shamt_i : in std_ulogic_vector(4 downto 0)
    )
    return std_ulogic_vector is
    begin
        return (31 downto 5 => '0') & shamt_i;
    end;

    function alu_type (
        exec_i : in exec_t
    )
    return std_ulogic is
    begin
        if exec_i = alu_add or exec_i = alu_sub 
            or exec_i = alu_and or exec_i = alu_or
            or exec_i = alu_xor or exec_i = alu_sll
            or exec_i = alu_sra or exec_i = alu_srl
            or exec_i = alu_slt or exec_i = alu_sltu then
            return '1';
        else
            return '0';
        end if;
    end;

    function branch_type (
        exec_i : in exec_t
    )
    return std_ulogic is
    begin
        if exec_i = cu_beq or exec_i = cu_bne 
            or exec_i = cu_blt or exec_i = cu_bge
            or exec_i = cu_bltu or exec_i = cu_bgeu
            or exec_i = cu_jal or exec_i = cu_jalr then
            return '1';
        else
            return '0';
        end if;
    end;

    function mmu_type (
        exec_i : in exec_t
    )
    return std_ulogic is
    begin
        if exec_i = mmu_sb or exec_i = mmu_sh 
            or exec_i = mmu_sw or exec_i = mmu_lb
            or exec_i = mmu_lh or exec_i = mmu_lw
            or exec_i = mmu_lbu or exec_i = mmu_lhu then
            return '1';
        else
            return '0';
        end if;
    end;
end package body;