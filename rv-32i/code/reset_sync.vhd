library ieee;
use ieee.std_logic_1164.all;

use work.exec_pkg.all;

entity reset_sync is
    port (
        clk_i : in std_ulogic;
        async_rst_n_i : in std_ulogic;

        rst_p_o : out std_ulogic
    );
end entity;

architecture rtl of reset_sync is
    signal connect_s : std_ulogic;
begin

    process (clk_i, async_rst_n_i)
    begin
        if async_rst_n_i = '0' then
            connect_s <= '1';
            rst_p_o <= '1';
        elsif rising_edge(clk_i) then
            connect_s <= '0';
            rst_p_o <= connect_s;
        end if;
    end process;        
end architecture;