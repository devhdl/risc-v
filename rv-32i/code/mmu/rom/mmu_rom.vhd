library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mmu_rom is
    generic (
        bytes_g : natural := 2**6
    );
    port (
        clk_i : in std_ulogic;
        rst_i : in std_ulogic;
        
        addr_i : in std_ulogic_vector(31 downto 0);
        we_i : in std_ulogic;
        instruction_o : out std_ulogic_vector(31 downto 0)
    );
end entity;

architecture rtl of mmu_rom is
    
begin
    p_main : process (clk_i)
        type mem_t is array (0 to bytes_g - 1) of std_ulogic_vector(7 downto 0);
        type inst_array_t is array (0 to 9) of std_ulogic_vector(31 downto 0);

        -- Default program (finding F7 for the Fibonacci sequence) consists of 9 instructions:
        -- addi x10, x0, 0  --->    x"00000513"
        -- addi x11, x0, 1  --->    x"00100593"
        -- addi x6, x0, 1   --->    x"00100313"
        -- addi x12, x0, 47  --->    x"02F00613"
        -- beq x6, x12, 0   --->    x"00C30063"
        -- addi x6, x6, 1   --->    x"00130313"
        -- add x7, x10, x11 --->    x"00B503B3"
        -- addi x10, x11, 0 --->    x"00058513"
        -- addi x11, x7, 0  --->    x"00038593"
        -- jal x0, -20      --->    x"FEDFF06F"
        constant instructions_c : inst_array_t := (x"00000513", x"00100593", x"00100313", x"02F00613",
                                                   x"00C30063", x"00130313", x"00B503B3", x"00058513",
                                                   x"00038593", x"FEDFF06F");

        variable mem_v : mem_t;
    begin
        if rising_edge(clk_i) then
            if rst_i then
                mem_v := (others => x"00");
                
                for i in 0 to instructions_c'length - 1 loop
                    mem_v(4*i + 0) := instructions_c(i)(7 downto 0);
                    mem_v(4*i + 1) := instructions_c(i)(15 downto 8);
                    mem_v(4*i + 2) := instructions_c(i)(23 downto 16);
                    mem_v(4*i + 3) := instructions_c(i)(31 downto 24);
                end loop;
            else
                instruction_o <= mem_v(to_integer(unsigned(addr_i))+3) &
                            mem_v(to_integer(unsigned(addr_i))+2) &
                            mem_v(to_integer(unsigned(addr_i))+1) &
                            mem_v(to_integer(unsigned(addr_i))+0);
            end if;
        end if;
    end process;
end architecture;
