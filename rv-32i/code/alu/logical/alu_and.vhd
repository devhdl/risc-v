library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;


entity alu_and is
    port(
        data1_i : in std_ulogic_vector;       -- 32-bit data from rs1
        data2_i : in std_ulogic_vector;       -- 32-bit data from rs2 / Immediate
        result_o : out std_ulogic_vector      -- 32-bit data to rd
    );
end;

architecture rtl of alu_and is

begin
    result_o <= (data1_i and data2_i);
end;
