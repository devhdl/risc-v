library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;


entity alu_sll is
    port(
        data1_i : in std_ulogic_vector;       -- 32-bit data from rs1  
        data2_i : in natural;                 -- amount of shifts
        result_o : out std_ulogic_vector      -- 32-bit data to rd
    );
end;

architecture rtl of alu_sll is

begin
    result_o <= std_ulogic_vector(shift_left(unsigned(data1_i), data2_i));
end;