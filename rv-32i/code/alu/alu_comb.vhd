library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

entity alu_comb is
    port (
        data1_i : in std_ulogic_vector;
        data2_i : in std_ulogic_vector;
        exec_i  : in exec_t;
        
        result_o : out std_ulogic_vector
    );
end entity;

architecture structural of alu_comb is
    type data_array_t is array (natural range 0 to 9) of std_ulogic_vector;

    constant default_result_c : std_ulogic_vector(result_o'range) := (others => '0');

    signal result_array_s : data_array_t(open)(result_o'range);

    signal add_data1_s : unsigned(data1_i'range);
    signal add_data2_s : unsigned(data2_i'range);
    signal add_result_s : unsigned(result_o'range);

    signal sub_data1_s : unsigned(data1_i'range);
    signal sub_data2_s : unsigned(data2_i'range);
    signal sub_result_s : unsigned(result_o'range);

begin
    add_data1_s <= unsigned(data1_i);
    add_data2_s <= unsigned(data2_i);
    result_array_s(0) <= std_ulogic_vector(add_result_s);

    sub_data1_s <= unsigned(data1_i);
    sub_data2_s <= unsigned(data2_i);
    result_array_s(1) <= std_ulogic_vector(sub_result_s);

    u_add : entity work.alu_add
    port map (
        data1_i => add_data1_s,
        data2_i => add_data2_s,
        result_o => add_result_s
    );

    u_sub : entity work.alu_sub
    port map (
        data1_i => sub_data1_s,
        data2_i => sub_data2_s,
        result_o => sub_result_s
    );

    u_and : entity work.alu_and
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        result_o => result_array_s(2)
    );

    u_or : entity work.alu_or
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        result_o => result_array_s(3)
    );

    u_xor : entity work.alu_xor
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        result_o => result_array_s(4)
    );

    u_sll : entity work.alu_sll
    port map (
        data1_i => data1_i,
        data2_i => to_integer(unsigned(data2_i)),
        result_o => result_array_s(5)
    );

    u_srl : entity work.alu_srl
    port map (
        data1_i => data1_i,
        data2_i => to_integer(unsigned(data2_i)),
        result_o => result_array_s(6)
    );

    u_sra : entity work.alu_sra
    port map (
        data1_i => data1_i,
        data2_i => to_integer(unsigned(data2_i)),
        result_o => result_array_s(7)
    );

    u_slt : entity work.alu_slt
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        result_o => result_array_s(8)
    );

    u_sltu : entity work.alu_sltu
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        result_o => result_array_s(9)
    );

    with exec_i select result_o <=
        result_array_s(0) when alu_add,
        result_array_s(1) when alu_sub,
        result_array_s(2) when alu_and,
        result_array_s(3) when alu_or,
        result_array_s(4) when alu_xor,
        result_array_s(5) when alu_sll,
        result_array_s(6) when alu_srl,
        result_array_s(7) when alu_sra,
        result_array_s(8) when alu_slt,
        result_array_s(9) when alu_sltu,
        default_result_c  when others;
end architecture;
