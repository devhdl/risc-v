library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;


entity alu_slt is
    port(
        data1_i : in std_ulogic_vector;       -- 32-bit data from rs1
        data2_i : in std_ulogic_vector;       -- 32-bit data from rs2 / Immediate
        result_o : out std_ulogic_vector      -- 32-bit data to rd
    );
end;

architecture rtl of alu_slt is

begin
    result_o <= std_ulogic_vector(to_unsigned(1, result_o'length)) when signed(data1_i) < signed(data2_i) else
                (result_o'range => '0');
end;
