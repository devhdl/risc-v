library ieee;
use ieee.std_logic_1164.all;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity alu is
    port (
        clk_i : in std_ulogic;
        rst_i : in std_ulogic;
        data1_i : in std_ulogic_vector;
        data2_i : in std_ulogic_vector;
        dest_i  : in reg_t;
        exec_i  : in exec_t;
        valid_i : in std_ulogic;
        
        valid_o : out std_ulogic;
        result_o : out std_ulogic_vector;
        dest_o   : out reg_t
    );
end entity;

architecture structural of alu is
    signal result_s : std_ulogic_vector(result_o'range);
begin
    u_alu_comb : entity work.alu_comb
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        exec_i => exec_i,

        result_o => result_s
    );

    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i then
                result_o <= (result_o'range => '0');
                dest_o <= 0;
                valid_o <= '0';
            else
                result_o <= result_s;
                dest_o <= dest_i;
                if valid_i then
                    valid_o <= alu_type(exec_i);
                else
                    valid_o <= '0';
                end if; 
            end if;
        end if;
    end process;
end architecture;