library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;


entity alu_sub is
    port(
        data1_i : in unsigned;       -- 32-bit data from rs1  
        data2_i : in unsigned;       -- 32-bit data from rs2 / Immediate
        result_o : out unsigned      -- 32-bit data to rd
    );
end;

architecture rtl of alu_sub is
begin
    result_o <= data1_i - data2_i;
end;