library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

entity cu_jal is
    port(
        offset_i : in std_ulogic_vector;
        pc_i : in std_ulogic_vector;
        
        bvalid_o : out std_ulogic;
        pc_o : out std_ulogic_vector;
        result_o : out std_ulogic_vector
    );
end;

architecture rtl of cu_jal is
begin
    pc_o <= std_ulogic_vector(signed(pc_i) + signed(offset_i));
    result_o <= std_ulogic_vector(unsigned(pc_i) + to_unsigned(4, pc_i'length));
    bvalid_o <= '1';
end;