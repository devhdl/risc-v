library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

entity cu_jalr is
    port(
        data1_i : in std_ulogic_vector;
        offset_i : in std_ulogic_vector;
        pc_i : in std_ulogic_vector;
        
        bvalid_o : out std_ulogic;
        pc_o : out std_ulogic_vector;
        result_o : out std_ulogic_vector
    );
end;

architecture rtl of cu_jalr is
signal pc_s : std_ulogic_vector(pc_o'range);
begin
    pc_s <= std_ulogic_vector(signed(data1_i) + signed(offset_i));
    pc_o <= pc_s(pc_o'length -1 downto 1) & '0';
    result_o <= std_ulogic_vector(unsigned(pc_i) + to_unsigned(4, result_o'length));
    bvalid_o <= '1';
end;