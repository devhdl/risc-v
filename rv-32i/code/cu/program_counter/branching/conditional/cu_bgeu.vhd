library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

entity cu_bgeu is
    port(
        data1_i : in std_ulogic_vector;       -- 32-bit data from rs1  
        data2_i : in std_ulogic_vector;       -- 32-bit data from rs2 / Immediate
        offset_i : in std_ulogic_vector;
        pc_i : in std_ulogic_vector;
        
        bvalid_o : out std_ulogic;
        pc_o : out std_ulogic_vector      -- 32-bit data to rd
    );
end;

architecture rtl of cu_bgeu is
begin
    pc_o <= std_ulogic_vector(signed(pc_i) + signed(offset_i));
    
    bvalid_o <=
        '1' when unsigned(data1_i) >= unsigned(data2_i) else
        '0';
end;