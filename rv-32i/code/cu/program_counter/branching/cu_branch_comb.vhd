library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

entity cu_branch_comb is
    port (
        data1_i : in std_ulogic_vector;
        data2_i : in std_ulogic_vector;
        offset_i : in std_ulogic_vector;
        pc_i : in std_ulogic_vector;

        exec_i  : in exec_t;
        
        result_o : out std_ulogic_vector;
        branch_valid_o : out std_ulogic;
        pc_o : out std_ulogic_vector
    );
end entity;

architecture structural of cu_branch_comb is
    type data_array_t is array (natural range 0 to 8) of std_ulogic_vector;
    type logic_array_t is array (natural range 0 to 8) of std_ulogic;

    constant default_c : std_ulogic_vector(result_o'range) := (others => '0');
    constant beq_c : natural := 0;
    constant bne_c : natural := 1;
    constant bge_c : natural := 2;
    constant blt_c : natural := 3;
    constant bgeu_c : natural := 4;
    constant bltu_c : natural := 5;
    constant jal_c : natural := 6;
    constant jalr_c : natural := 7;
    constant other_c : natural := 8;

    signal op_s : natural;

    signal b_valid_array_s : logic_array_t(open);
    signal pc_array_o_s : data_array_t(open)(pc_o'range);

    signal result_jal_s : std_ulogic_vector(result_o'range);
    signal result_jalr_s : std_ulogic_vector(result_o'range);

begin
    u_beq : entity work.cu_beq
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(0),
        pc_o => pc_array_o_s(0)
    );

    u_bne : entity work.cu_bne
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(1),
        pc_o => pc_array_o_s(1)
    );


    u_bge : entity work.cu_bge
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(2),
        pc_o => pc_array_o_s(2)
    );

    u_blt : entity work.cu_blt
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(3),
        pc_o => pc_array_o_s(3)
    );

    u_bgey : entity work.cu_bgeu
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(4),
        pc_o => pc_array_o_s(4)
    );

    u_bltu : entity work.cu_bltu
    port map (
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(5),
        pc_o => pc_array_o_s(5)
    );

    u_jal : entity work.cu_jal
    port map (
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(6),
        pc_o => pc_array_o_s(6),
        result_o => result_jal_s
    );

    u_jalr : entity work.cu_jalr
    port map (
        data1_i => data1_i,
        offset_i => offset_i,
        pc_i => pc_i,
        bvalid_o => b_valid_array_s(7),
        pc_o => pc_array_o_s(7),
        result_o => result_jalr_s
    );

    with exec_i select result_o <=
        result_jal_s when cu_jal,
        result_jalr_s when cu_jalr,
        default_c  when others;

    pc_array_o_s(other_c) <= (pc_array_o_s(other_c)'range => '0');
    b_valid_array_s(other_c) <= '0';

    with exec_i select op_s <=
        beq_c when cu_beq,
        bne_c when cu_bne,
        bge_c when cu_bge,
        blt_c when cu_blt,
        bgeu_c when cu_bgeu,
        bltu_c when cu_bltu,
        jal_c when cu_jal,
        jalr_c when cu_jalr,
        other_c  when others;

    pc_o <= pc_array_o_s(op_s);
    branch_valid_o <= b_valid_array_s(op_s);
    
end architecture;
