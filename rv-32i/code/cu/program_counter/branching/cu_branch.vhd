library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_branch is
    port(
        rst_i   : in std_ulogic;              -- reset
        clk_i   : in std_ulogic;              -- system clock

        valid_i : in std_ulogic;
        dest_i : in reg_t;

        data1_i : in std_ulogic_vector;
        data2_i : in std_ulogic_vector;
        offset_i : in std_ulogic_vector;
        pc_i : in std_ulogic_vector;

        exec_i  : in exec_t;
        
        result_o : out std_ulogic_vector;
        branch_valid_o : out std_ulogic;
        valid_o : out std_ulogic;
        pc_o : out std_ulogic_vector;
        dest_o : out reg_t
    );
end;

architecture structural of cu_branch is
    signal result_s : std_ulogic_vector(result_o'range);
    signal pc_s : std_ulogic_vector(pc_o'range);
    signal branch_valid_s : std_ulogic;

begin
    u_branch_comb : entity work.cu_branch_comb
    port map(
        data1_i => data1_i,
        data2_i => data2_i,
        offset_i => offset_i,
        pc_i => pc_i,
        exec_i => exec_i,

        result_o => result_s,
        branch_valid_o => branch_valid_s,
        pc_o => pc_s
    );

    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i then
                result_o <= (result_o'range => '0');
                branch_valid_o <= '0';
                valid_o <= '0';
                pc_o <= (pc_o'range => '0');
                dest_o <= 0;
            else
                result_o <= result_s;
                if valid_i then
                    branch_valid_o <= branch_valid_s;
                    valid_o <= branch_type(exec_i);
                else
                    branch_valid_o <= '0';
                    valid_o <= '0';
                end if;
                pc_o <= pc_s;
                dest_o <= dest_i;
            end if;
        end if;
    end process;
end;