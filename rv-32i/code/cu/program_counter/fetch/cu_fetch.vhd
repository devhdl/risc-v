library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use work.exec_pkg.all;

entity cu_fetch is
    generic (
        delay_cycles_g : natural := 4   -- 3 + no of cycles the memory needs to prepare instructions.
    );
    port (
        rst_i   : in std_ulogic;              -- reset
        clk_i   : in std_ulogic;              -- system clock

        pc_i   : in std_ulogic_vector;
        valid_i  : in std_ulogic;
        branch_valid_i : in std_ulogic;

        ext_addr_o : out std_ulogic_vector;
        ext_we_o   : out std_ulogic;        
        ext_instruction_i : in  std_ulogic_vector;

        instruction_o : out std_ulogic_vector;
        pc_o : out std_ulogic_vector;
        valid_o : out std_ulogic
    );
end entity;

architecture rtl of cu_fetch is
signal pc_p4_s : std_ulogic_vector (pc_i'range);        -- PC + 4
signal valid_s : std_ulogic;

attribute mark_debug : string;
    
begin
    ext_we_o <= '0';

    p_fetch : process(clk_i)
        type exec_array_t is array (delay_cycles_g - 1 downto 0) of exec_t;
        type delay_reg_array_t is array (delay_cycles_g - 1 downto 0) of reg_t;
        variable valid_delay_v : std_ulogic_vector(delay_cycles_g - 1 downto 0);
        variable addr_v : std_ulogic_vector(pc_i'range);
    begin
        if rising_edge(clk_i) then
            if rst_i then
                valid_delay_v := (others => '0');
                valid_s <= '1';
                valid_o <= '0';
                ext_addr_o <= (ext_addr_o'range => '0');
                pc_o <= (pc_o'range => '0');
                instruction_o <= (instruction_o'range => '0');
            else
                if valid_i or valid_s then
                    valid_s <= '0';

                    if branch_valid_i = '1' then
                        addr_v := pc_i;
                    elsif valid_s = '1' then
                        addr_v := (pc_i'range => '0');
                    else
                        addr_v := pc_p4_s;
                    end if;
                    ext_addr_o <= addr_v(pc_i'range);
                end if;

                if valid_delay_v(delay_cycles_g - 1) then
                    valid_o <= '1';
                    instruction_o <= ext_instruction_i;
                    pc_o <= addr_v;
                    pc_p4_s <= std_ulogic_vector(unsigned(addr_v) + to_unsigned(4, addr_v'length)); 
                else
                    valid_o <= '0';
                end if;
            
                for i in valid_delay_v'range loop
                    if i = 0 then
                        valid_delay_v(i) := valid_s or valid_i;
                    else
                        valid_delay_v(i) := valid_delay_v(i-1);
                    end if;
                end loop;
            end if;
        end if;
    end process;

    
end architecture;
