library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_decoder_comb is
    port(
        instruction_i : in std_logic_vector;
        data_reg_i : in reg_array_t;            

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture structural of cu_decoder_comb is
    type io_array_t is array (natural range 0 to 5) of std_ulogic_vector;
    type operation_array_t is array (natural range 0 to 5) of exec_t;
    type dest_array_t is array (natural range 0 to 5) of reg_t;

    signal op_array_s : operation_array_t(open);
    signal data1_array_s : io_array_t(open)(data1_o'range);
    signal data2_array_s : io_array_t(open)(data2_o'range);
    signal offset_array_s : io_array_t(open)(offset_o'range);
    signal destination_array_s : dest_array_t(open);

    alias opcode is instruction_i(6 downto 0);

    constant type_r_c : natural := 0;
    constant type_i_c : natural := 1;
    constant type_s_c : natural := 2;
    constant type_b_c : natural := 3;
    constant type_uj_c : natural := 4;
    constant type_u_c : natural := 5;

    signal type_s : natural;

begin
    u_type_r : entity work.cu_type_r
    port map (
        instruction_i => instruction_i,
        data_reg_i => data_reg_i,

        op_o => op_array_s(0),
        data1_o => data1_array_s(0),
        data2_o => data2_array_s(0),
        offset_o => offset_array_s(0),
        dest_o => destination_array_s(0)
    );

    u_type_i : entity work.cu_type_i
    port map (
        instruction_i => instruction_i,
        data_reg_i => data_reg_i,

        op_o => op_array_s(1),
        data1_o => data1_array_s(1),
        data2_o => data2_array_s(1),
        offset_o => offset_array_s(1),
        dest_o => destination_array_s(1)
    );

    u_type_s : entity work.cu_type_s
    port map (
        instruction_i => instruction_i,
        data_reg_i => data_reg_i,

        op_o => op_array_s(2),
        data1_o => data1_array_s(2),
        data2_o => data2_array_s(2),
        offset_o => offset_array_s(2),
        dest_o => destination_array_s(2)
        
    );

    u_type_b : entity work.cu_type_b
    port map (
        instruction_i => instruction_i,
        data_reg_i => data_reg_i,

        op_o => op_array_s(3),
        data1_o => data1_array_s(3),
        data2_o => data2_array_s(3),
        offset_o => offset_array_s(3),
        dest_o => destination_array_s(3)
    );

    u_type_uj : entity work.cu_type_uj
    port map (
        instruction_i => instruction_i,

        op_o => op_array_s(4),
        data1_o => data1_array_s(4),
        data2_o => data2_array_s(4),
        offset_o => offset_array_s(4),
        dest_o => destination_array_s(4)
    );

    u_type_u : entity work.cu_type_u
    port map (
        instruction_i => instruction_i,

        op_o => op_array_s(5),
        data1_o => data1_array_s(5),
        data2_o => data2_array_s(5),
        offset_o => offset_array_s(5),
        dest_o => destination_array_s(5)
    );

    type_s <=   type_r_c when opcode = op_op else
                type_i_c when opcode = op_imm_op or
                        opcode = load_op or
                        opcode = jalr_op else
                type_s_c when opcode = store_op else
                type_b_c when opcode = branch_op else
                type_uj_c when opcode = jal_op else
                type_u_c;

    op_o <= op_array_s(type_s);
    data1_o <= data1_array_s(type_s);
    data2_o <= data2_array_s(type_s);
    offset_o <= offset_array_s(type_s);    
    dest_o <= destination_array_s(type_s);
end;