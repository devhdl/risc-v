library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_decoder is
    port(
        rst_i   : in std_ulogic;              -- reset
        clk_i   : in std_ulogic;              -- system clock

        valid_i : in std_ulogic;

        pc_i : in std_ulogic_vector;
        instruction_i : in std_logic_vector;
        data_reg_i : in reg_array_t;            

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t;
        pc_o : out std_ulogic_vector;
        valid_o : out std_ulogic
    );
end;

architecture structural of cu_decoder is
    signal op_s : exec_t;
    signal data1_s : std_logic_vector(data1_o'range);
    signal data2_s : std_logic_vector(data2_o'range);
    signal offset_s : std_logic_vector(offset_o'range);
    signal dest_s : reg_t;

begin
    u_decoder_comb : entity work.cu_decoder_comb
    port map(
        instruction_i => instruction_i,
        data_reg_i => data_reg_i,

        op_o => op_s,
        data1_o => data1_s,
        data2_o => data2_s,
        offset_o => offset_s,
        dest_o => dest_s
    );

    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i then
                op_o <= none;
                data1_o <= (data1_o'range => '0');
                data2_o <= (data2_o'range => '0');
                offset_o <= (offset_o'range => '0');
                dest_o <= 0;

                pc_o <= (pc_o'range => '0');
                valid_o <= '0';
            else
                op_o <= op_s;
                data1_o <= data1_s;
                data2_o <= data2_s;
                offset_o <= offset_s;
                dest_o <= dest_s;

                pc_o <= pc_i;
                valid_o <= valid_i;
            end if;
        end if;
    end process;
end;