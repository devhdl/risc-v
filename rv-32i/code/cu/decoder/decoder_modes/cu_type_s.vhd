library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_type_s is
    port(
        instruction_i : in std_logic_vector;
        data_reg_i : in reg_array_t;            

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture rtl of cu_type_s is

alias funct3 : std_ulogic_vector is instruction_i(14 downto 12);
alias reg1 : std_ulogic_vector is instruction_i(19 downto 15);
alias reg2 : std_ulogic_vector is instruction_i(24 downto 20);
alias immediate_h : std_ulogic_vector is instruction_i(31 downto 25);
alias immediate_l : std_ulogic_vector is instruction_i(11 downto 7);
alias opcode : std_ulogic_vector is instruction_i(6 downto 0);

signal imm12_s : std_ulogic_vector(11 downto 0);

begin 
    data1_o <= data_reg_i(to_reg_t(reg1));  -- Address Base
    data2_o <= data_reg_i(to_reg_t(reg2));  -- Data
    imm12_s <=  immediate_h & immediate_l;
    offset_o <= imm12_to_32b(imm12_s);  -- Offset to address base
    dest_o <= 0;


    op_o <=
        mmu_sb when funct3 = "000" else
        mmu_sh when funct3 = "001" else
        mmu_sw when funct3 = "010" else
        none;    
end;