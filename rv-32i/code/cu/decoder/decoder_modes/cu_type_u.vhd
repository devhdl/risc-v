library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_type_u is
    port(
        instruction_i : in std_logic_vector;        

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture rtl of cu_type_u is

alias regd : std_ulogic_vector is instruction_i(11 downto 7);
alias imm20 :std_ulogic_vector is instruction_i(31 downto 12);
alias opcode :  std_ulogic_vector is instruction_i(6 downto 0);

begin 
   data1_o <= imm20_to_32b_ui(imm20);
   data2_o <= (data2_o'range => '0');
   offset_o <= (offset_o'range => '0');
   dest_o <= to_reg_t(regd);

   with opcode select op_o <=
        -- lui when lui_op,
        -- auipc when auipc_op,
        none when others;    
end;