library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_type_uj is
    port(
        instruction_i : in std_logic_vector;         

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture rtl of cu_type_uj is

    alias regd : std_ulogic_vector is instruction_i(11 downto 7);
    alias opcode :  std_ulogic_vector is instruction_i(6 downto 0);
    alias imm_20 :std_ulogic is instruction_i(31);
    alias imm_19dt12 : std_ulogic_vector is instruction_i(19 downto 12);
    alias imm_11 : std_ulogic is instruction_i(20);
    alias imm_10dt1 : std_ulogic_vector is instruction_i(30 downto 21);

    signal imm20_s : std_ulogic_vector(20 downto 0);
    
    begin 
        data1_o <= (data1_o'range => '0');
        data2_o <= (data2_o'range => '0');
        imm20_s <= imm_20 & imm_19dt12 & imm_11 & imm_10dt1 & '0';
        offset_o <= std_ulogic_vector(to_signed(to_integer(signed(imm20_s)),32));
        dest_o <= to_reg_t(regd);

    
        with opcode select op_o <=
            cu_jal when jal_op,
            none when others;    
end;