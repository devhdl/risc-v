library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_type_i is
    port(
        instruction_i : in std_logic_vector;
        data_reg_i : in reg_array_t;            

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture rtl of cu_type_i is
constant default_data_c : std_ulogic_vector(data2_o'range) := (others => '0');
constant default_offset_c : std_ulogic_vector(offset_o'range) := (others => '0');

alias opcode : std_ulogic_vector is instruction_i(6 downto 0);
alias regd : std_ulogic_vector is instruction_i(11 downto 7);
alias funct3 : std_ulogic_vector is instruction_i(14 downto 12);
alias reg1 : std_ulogic_vector is instruction_i(19 downto 15);
alias immediate : std_ulogic_vector is instruction_i(31 downto 20);
alias shamt : std_ulogic_vector is instruction_i(24 downto 20);
alias funct7 : std_ulogic_vector is instruction_i(31 downto 25);

signal encoding : std_ulogic_vector (4 downto 0);

begin
    encoding <=
        "01" & funct3 when funct3 = "101" and funct7 = "0100000" and opcode = op_imm_op else --SRA case
        "00" & funct3 when opcode = op_imm_op else  --Remaining ALU cases
        "10" & funct3 when opcode = load_op else  --Load operations
        "11" & funct3 when opcode = jalr_op else  --JALR operation
        "11111";
        
    data1_o <= data_reg_i(to_reg_t(reg1));

    with encoding(4 downto 3) select data2_o <=
        shamt_to_32b(shamt) when "01",
        imm12_to_32b(immediate) when "00",
        default_data_c when others;

    with encoding(4 downto 3) select offset_o <=
        imm12_to_32b(immediate) when "10",
        imm12_to_32b(immediate) when "11",
        default_data_c when others;

    dest_o <= to_reg_t(regd);


    with encoding select op_o <=
        alu_add when "00000",
        alu_sll when "00001",
        -- slt when "00010",
        -- sltu when "00011",
        alu_xor when "00100",
        alu_srl when "00101",
        alu_sra when "01101",
        alu_or when "00110",
        alu_and when "00111",
        mmu_lb when "10000",
        mmu_lh when "10001",
        mmu_lw when "10010",
        mmu_lbu when "10100",
        mmu_lhu when "10101",
        cu_jalr when "11000",
        none when others;    
end;