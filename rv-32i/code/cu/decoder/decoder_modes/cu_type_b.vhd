library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_type_b is
    port(
        instruction_i : in std_logic_vector;
        data_reg_i : in reg_array_t;            

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture rtl of cu_type_b is

alias funct3 : std_ulogic_vector is instruction_i(14 downto 12);
alias reg1 : std_ulogic_vector is instruction_i(19 downto 15);
alias reg2 : std_ulogic_vector is instruction_i(24 downto 20);
alias immediate_12 : std_ulogic is instruction_i(31);
alias immediate_11 : std_ulogic is instruction_i(7);
alias immediate_10dt5 : std_ulogic_vector is instruction_i(30 downto 25);
alias immediate_4dt1 : std_ulogic_vector is instruction_i(11 downto 8);
alias opcode : std_ulogic_vector is instruction_i(6 downto 0);

signal imm12_s : std_ulogic_vector (12 downto 0);

begin 
    data1_o <= data_reg_i(to_reg_t(reg1));  -- Argument 1
    data2_o <= data_reg_i(to_reg_t(reg2));  -- Argument 2
    imm12_s <= immediate_12 & immediate_11 & immediate_10dt5 & immediate_4dt1 & '0'; 
    offset_o <= std_ulogic_vector(to_signed(to_integer(signed(imm12_s)),32)); -- Branching offset
    dest_o <= 0;


    op_o <=
        cu_beq when funct3 = "000" else
        cu_bne when funct3 = "001" else
        cu_blt when funct3 = "100" else
        cu_bge when funct3 = "101" else
        cu_bltu when funct3 = "110" else
        cu_bgeu when funct3 = "111" else
        none;    
end;