library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;
use work.decoder_pkg.all;

entity cu_type_r is
    port(
        instruction_i : in std_logic_vector;
        data_reg_i : in reg_array_t;            

        op_o : out exec_t;
        data1_o : out std_ulogic_vector;
        data2_o : out std_ulogic_vector;
        offset_o : out std_ulogic_vector;
        dest_o  : out reg_t
    );
end;

architecture rtl of cu_type_r is
alias opcode : std_ulogic_vector is instruction_i(6 downto 0);
alias regd : std_ulogic_vector is instruction_i(11 downto 7);
alias funct3 : std_ulogic_vector is instruction_i(14 downto 12);
alias reg1 : std_ulogic_vector is instruction_i(19 downto 15);
alias reg2 : std_ulogic_vector is instruction_i(24 downto 20);
alias funct7 : std_ulogic_vector is instruction_i(31 downto 25);

signal encoding : std_ulogic_vector (3 downto 0);

begin
    data1_o <= data_reg_i(to_reg_t(reg1));
    data2_o <= data_reg_i(to_reg_t(reg2));
    offset_o <= (offset_o'range => '0');
    dest_o <= to_reg_t(regd);

    encoding <= 
        '0' & funct3 when funct7 = "0000000" else
        '1' & funct3 when funct7 = "0100000" else
        "1111";

    with encoding select op_o <=
        alu_add when "0000",
        alu_sub when "1000",
        alu_sll when "0001",
        -- slt when "0010",
        -- sltu when "0011",
        alu_xor when "0100",
        alu_srl when "0101",
        alu_sra when "1101",
        alu_or when "0110",
        alu_and when "0111",
        none when others; 
end;