library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.ALL;

use work.exec_pkg.all;

entity cu_registers is
    port(
        rst_i   : in std_ulogic;              -- reset
        clk_i   : in std_ulogic;              -- system clock

        data_i : in std_ulogic_vector;        -- 32-bit data
        reg_i   : in reg_t;                   -- target register
        valid_i : in std_ulogic;              -- valid write

        data_all_o : out reg_array_t          -- 32 data of 32 registers
    );
end;

architecture rtl of cu_registers is
begin
    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i then
                for i in reg_t loop
                    data_all_o(i) <= (data_all_o(0)'range => '0');
                end loop;  -- i
            else
                if valid_i = '1' then
                    data_all_o(reg_i) <= data_i;
                end if;
                data_all_o(0) <= (data_all_o(0)'range => '0');
            end if;
        end if;
    end process;
end;