library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.exec_pkg.all;

entity rv_32i is
    port (
        clk_i : in std_ulogic;
        rst_n_i : in std_ulogic
    );
end entity;

architecture structural of rv_32i is
signal rst_p_s : std_ulogic;

signal ext_addr_s : std_ulogic_vector(31 downto 0);
signal ext_we_s : std_ulogic;
signal ext_instruction_s: std_ulogic_vector(31 downto 0);

signal pc_bf_s : std_ulogic_vector(31 downto 0);
signal branch_valid_s : std_ulogic;
signal valid_bf_s : std_ulogic;

signal pc_fd_s : std_ulogic_vector(31 downto 0);
signal instruction_s : std_ulogic_vector(31 downto 0);
signal valid_fd_s : std_ulogic;

signal data_reg_s : reg_array_t(reg_t)(31 downto 0);
signal data1_s : std_ulogic_vector(31 downto 0);
signal data2_s : std_ulogic_vector(31 downto 0);
signal offset_s : std_ulogic_vector(31 downto 0);
signal dest_s : reg_t;
signal exec_s : exec_t;
signal valid_s : std_ulogic;
signal pc_db_s : std_ulogic_vector(31 downto 0);

signal result_s : std_ulogic_vector(31 downto 0);
signal dest_alu_s : reg_t;
signal valid_alu_s : std_ulogic;

signal data_branch_s : std_ulogic_vector(31 downto 0);
signal dest_branch_s : reg_t;
signal valid_fetch_s : std_ulogic;

signal valid_reg_s : std_ulogic;
signal reg_s : reg_t;
signal data_reg_i_s : std_ulogic_vector(31 downto 0);

attribute keep : string;
attribute keep of rst_p_s : signal is "true";
attribute keep of ext_addr_s : signal is "true";
attribute keep of ext_we_s : signal is "true";
attribute keep of ext_instruction_s : signal is "true";
attribute keep of pc_bf_s : signal is "true";
attribute keep of branch_valid_s : signal is "true";
attribute keep of pc_fd_s : signal is "true";
attribute keep of instruction_s : signal is "true";
attribute keep of valid_fd_s : signal is "true";
attribute keep of data_reg_s : signal is "true";
attribute keep of data1_s : signal is "true";
attribute keep of data2_s : signal is "true";
attribute keep of offset_s : signal is "true";
attribute keep of dest_s : signal is "true";
attribute keep of exec_s : signal is "true";
attribute keep of valid_s : signal is "true";
attribute keep of pc_db_s : signal is "true";
attribute keep of result_s : signal is "true";
attribute keep of dest_alu_s : signal is "true";
attribute keep of valid_alu_s : signal is "true";
attribute keep of data_branch_s : signal is "true";
attribute keep of dest_branch_s : signal is "true";
attribute keep of valid_fetch_s : signal is "true";
attribute keep of valid_reg_s : signal is "true";
attribute keep of reg_s : signal is "true";
attribute keep of data_reg_i_s : signal is "true";

begin

u_reset_sync : entity work.reset_sync
port map (
    clk_i => clk_i,
    async_rst_n_i => rst_n_i,
    rst_p_o => rst_p_s
);

u_fetch : entity work.cu_fetch
port map(
    clk_i => clk_i,
    rst_i => rst_p_s,

    ext_addr_o => ext_addr_s,
    ext_we_o => ext_we_s,
    ext_instruction_i => ext_instruction_s,

    pc_i => pc_bf_s,
    branch_valid_i => branch_valid_s,
    valid_i => valid_fetch_s,

    pc_o => pc_fd_s,
    instruction_o => instruction_s,
    valid_o => valid_fd_s 
);

u_rom : entity work.mmu_rom
port map(
    clk_i => clk_i,
    rst_i => rst_p_s,

    addr_i => ext_addr_s,
    we_i => ext_we_s,
    instruction_o => ext_instruction_s
);

u_decoder : entity work.cu_decoder
port map(
    clk_i => clk_i,
    rst_i => rst_p_s,

    instruction_i => instruction_s,
    valid_i => valid_fd_s,
    pc_i => pc_fd_s,

    data_reg_i => data_reg_s,

    data1_o => data1_s,
    data2_o => data2_s,
    offset_o => offset_s,
    dest_o => dest_s,
    valid_o => valid_s,
    op_o => exec_s,
    pc_o => pc_db_s
);

u_alu : entity work.alu
port map(
    clk_i => clk_i,
    rst_i => rst_p_s,

    data1_i => data1_s,
    data2_i => data2_s,
    dest_i => dest_s,
    exec_i => exec_s,
    valid_i => valid_s,

    result_o => result_s,
    dest_o => dest_alu_s,
    valid_o => valid_alu_s
);

u_branching : entity work.cu_branch
port map(
    clk_i => clk_i,
    rst_i => rst_p_s,

    data1_i => data1_s,
    data2_i => data2_s,
    offset_i => offset_s,
    dest_i => dest_s,
    exec_i => exec_s,
    valid_i => valid_s,
    pc_i => pc_db_s,

    pc_o => pc_bf_s,
    branch_valid_o => branch_valid_s,

    result_o => data_branch_s,
    dest_o => dest_branch_s,
    valid_o => valid_bf_s
);

u_reg : entity work.cu_registers
port map(
    clk_i => clk_i,
    rst_i => rst_p_s,

    valid_i => valid_reg_s,
    data_i => data_reg_i_s,
    reg_i => reg_s,
    data_all_o => data_reg_s
);

valid_reg_s <= 
    '1' when valid_bf_s or valid_alu_s else
    '0';

data_reg_i_s <= 
    data_branch_s when valid_bf_s else
    result_s when valid_alu_s else
    (data_reg_i_s'range => '0');

reg_s <=
    dest_alu_s when valid_alu_s else
    dest_branch_s when valid_bf_s else
    0;

valid_fetch_s <= valid_bf_s or valid_alu_s;

end architecture;
