library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.io_pkg.all;

entity input_pin is
    port (
        input_i : in std_ulogic_vector;
        input_state_o : out data_array_t
    );
end entity;

architecture rtl of input_pin is

constant array_len_c : natural := input_i'length / 32 + 1;
signal input_state_s : data_array_t(0 to array_len_c - 1)(31 downto 0);
signal input_s :std_ulogic_vector(array_len_c * 32 - 1  downto 0);

begin
    input_s <= (input_s'length - 1 downto input_i'length => '0') & input_i;
    repakage :for j in 0 to array_len_c - 1 generate
        input_state_s(j) <= input_s(31+32*j downto 32*j);
    end generate;

    input_state_o <= input_state_s;

end architecture;