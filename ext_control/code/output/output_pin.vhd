library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.io_pkg.all;

entity output_pin is
    port (
        output_state_i : in data_array_t;
        output_o : out std_ulogic_vector
        
    );
end entity;

architecture rtl of output_pin is

constant no_output_c : natural := output_o'length;

begin    

    output :for j in 0 to no_output_c - 1 generate
        output_o(j) <= output_state_i(j/32)(j mod 32);
    end generate;

end architecture;