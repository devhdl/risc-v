library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

package io_pkg is
    type data_array_t is array (natural range <>) of std_ulogic_vector;
end package;
