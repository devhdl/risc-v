library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.io_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity output_tb is
    generic (runner_cfg : string);
end;

architecture tb of output_tb is
    signal output_s : std_logic_vector(4 downto 0);
    signal output_state_s : data_array_t(0 to 0)(31 downto 0);

begin
    u_output1 : entity work.output_pin
    port map (
        output_state_i => output_state_s,
        output_o => output_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test when number of outputs is 5") then
                output_state_s(0)(4 downto 0) <= "10101";
                wait for 1 ns;
                check(output_s = "10101", "the output of the 5 pins are 10101");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;