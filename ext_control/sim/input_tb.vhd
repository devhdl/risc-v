library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.io_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity input_tb is
    generic (runner_cfg : string);
end;

architecture tb of input_tb is
    signal input1_s : std_logic_vector(4 downto 0);
    signal output1_s : data_array_t(0 to 0)(31 downto 0);

begin
    u_input1 : entity work.input_pin
    port map (
        input_i => input1_s,
        input_state_o => output1_s
    );

    p_test : process is
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("Test when number of inputs is 5") then
                input1_s <= "10101";
                wait for 1 ns;
                check(output1_s(0) = (31 downto 5 => '0') & "10101", "the input of the 5 pins are 10101");
            end if;
        end loop;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;