# Style Guide

The following is the recommended style for coding in VHDL:
- Put folders into a sensible structure, ideally one that reflects the structure of the design
- Use the .vhd file extension for VHDL files
- Use one .vhd file per design entity
- Name the .vhd file with the same name as the design entity
- Use lowercase file and folder names
- Only use libraries and import packages that are required by the design entity
- Use IEEE standard packages over 3rd party packages
- Code in lowercase, unless using uppercase for a particular scenario makes the code clearer
- Use VHDL-2008 features where possible (i.e. unconstrained vector widths, read from output ports, etc.)
- Avoid code duplication (use entities, functions, subprograms etc.)
- Use abstraction as much as possible (use the most appropriate type, or create a new one)
- Use modulatiry as much as possible (break down into small design entities)
- Give input ports the suffix _i
- Give output ports the suffix _o
- Give tristate ports the suffix _z
- Give generics the suffix _g
- Use only an end to terminate a statement, unless other words are required for clarity
- Only create one architecture per entity
- Only use behavioural, structural, rtl or 
- Give constants the suffix _c
- Give signals the suffix _s
- Do not use labels if they are not required
- Only include signals that are required in process sensitivity lists (i.e. clock signals)
- Declare constants pr signals that are only used by a process in that process
- Use variables instead of signals where possible
- Give variables the suffix _v
- Use entity instantiation, not component instantiation
- Perform type conversions in port maps if required

More rules may be added to this style guide later.

The most important thing to remember is to keep it simple!