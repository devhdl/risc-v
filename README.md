# RISC-V

A VHDL implementation of a RISC-V core. See Wiki: https://gitlab.com/devhdl/risc-v/-/wikis/Simple-One-Stage-rv-32i-Design

# Currently Available
- ALU
- Branching Unit
- Fetch Logic
- A built-in ROM
- Register File
- RV32-i Decoder

# Under Development
- Load/Store Unit
- Pipelining mechanics
- Branching predictions
- Efficient data-path
- More rigorous verification methods using SystemVerilog/OSVVM