from pathlib import Path
import json
import os

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


def create_vhd_list(ref_, target_, file_ext=".vhd"):
	""" This creates a list of vhd files from a reference directory.

        Parameters
        ----------
        ref_: string
            This is the relative path of the reference point from the running directory.
			If the reference point is the running directory of this function, input "./"
			in this field.
			Example, if the reference point is the previous directory from the running 
			directory, input "../" 
        target_: string
            The target directory relative to the reference point to execute the search.
			Example, if you want to search file A in the parent directory of the 
			running directory of this function, with the reference point as the 
			parent directory, then ref_ = "../" and target_ = "A".
		file_ext: string [optional]
			This is the file extension we want to search.
			Example, if we want to find .vhd files, then input file_ext = ".vhd". 

        Returns
        -------
        vhd_files: list
            Returns a list all the paths of the files we searched in the target with
			respect to the specific reference point.
    """
	target = target_
	if ref_ == "./":
		ref = None
		SRC_PATH = target
	else:
		ref = ref_
		SRC_PATH = ref + target

	print("Creating sub-directory lists...")
	sub_dir = [x[0] for x in os.walk(SRC_PATH)]

	vhd_files = []

	print("Searching begins...")
	for directory in sub_dir:
		print("Searching " + directory)
		for file in os.listdir(directory):
			if file.endswith(".vhd"):
				vhd_paths = os.path.join(directory, file)
				if ref is None:
					ref_len = 0
				else:
					ref_len = len(ref)
				print(color.BOLD + color.GREEN + "Found    " + color.END, vhd_paths[ref_len:])
				vhd_files.append(vhd_paths[ref_len:])

	print()
	print(color.BOLD + "Found " + str(len(vhd_files)) + " files..." + color.END)
	print(color.BOLD + color.GREEN + "Done !" + color.END)
	print()
	print()
	return vhd_files

def create_hdl_prj_json(target_loc, target_file):
	""" This creates the hdl-prj.json file for ghdl-ls.

        Parameters
        ----------
        target_loc: string
			The location of the json file with respect to the running directory.
        target_file: array (string)
            The target directories relative to the target location.
		
        Returns
        -------
        None
    """

	main_dict = {}
	options = {}

	options["ghdl_analysis"] = [
		"--workdir=work",
		"--ieee=synopsys",
		"-fexplicit",
		"--std=08",
		"-Pvunit_out/ghdl/libraries/vunit_lib",
		"-Pvunit_out/ghdl/libraries/osvvm"
	]

	main_dict["options"] = options

	files = []

	vhd_list = []
	for dir in target_file:
		vhd_list = vhd_list + create_vhd_list(target_loc, dir)
	
	print()
	print("Generating hdl-prj.json file...")
	for vhd in vhd_list:
		file_detail = {}
		file_detail["file"] = vhd
		file_detail["language"] = "vhdl"
		files.append(file_detail)

	main_dict["files"] = files

	main_dict = json.dumps(main_dict, indent = 4)

	with open(target_loc + "hdl-prj.json", "w") as outfile:
		outfile.write(main_dict)

	print(color.BOLD + color.GREEN + "Done !" + color.END)

# Running this file will generate the json file.
if __name__ == "__main__":
	create_hdl_prj_json("../", ["rv-32i", "ext_control"])