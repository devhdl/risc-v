from pathlib import Path
from vunit import VUnit
from file_finder import create_vhd_list

VU = VUnit.from_argv()
VU.add_osvvm()

SRC_PATH = Path(__file__).parent / ".." / "rv-32i"

vhd_list = create_vhd_list("./", SRC_PATH)

VU.add_library("lib").add_source_files(
    vhd_list
)

VU.set_compile_option("ghdl.a_flags", ["-frelaxed", "--std=08"])
VU.set_sim_option("ghdl.elab_flags", ["-frelaxed", "--std=08"])

VU.main()